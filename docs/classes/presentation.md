### classes[0] = "Presentation"

#### Laboratório de Bioinformática 2019-2020

![Logo EST](presentation_assets/logo-ESTB.png)

<center>Francisco Pina Martins</center>

[@FPinaMartins](https://twitter.com/FPinaMartins)

---

## Practical info

<ul>
  <li class="fragment">Schedule: Mondays - 09:30 to 13:30, **Room 1.12**</li>
  <li class="fragment">Questions? f.pina.martins@estbarreiro.ips.pt</li>
  <li class="fragment">~~[Moodle](http://moodle.ips.pt/)~~</li>
</ul>

---

## Avaliação

<ul>
  <li class="fragment"><font color="green">Contínua</font></li>
    <ul>
      <li class="fragment"><font color="orange">20% - Contexto sala de aula:</font/></li>
      <ul>
        <li class="fragment"><font color="red">10% - Atitude</font></li>
        <li class="fragment"><font color="red">5% - Participação</font></li>
        <li class="fragment"><font color="red">5% - Postura/interesse</font></li>
      </ul>
      <li class="fragment"><font color="orange">20% - Relatório mid-term</font/></li>
      <li class="fragment"><font color="orange">60% - Relatório Final</font/></li>
      <ul>
        <li class="fragment"><font color="red">10% - Relatório individual</font></li>
        <li class="fragment"><font color="red">30% - Trabalho prático</font></li>
        <li class="fragment"><font color="red">20% - Apresentação</font></li>
      </ul>
      <li class="fragment"><font color="orange">Sem notas mínimas</font/></li>
    </ul>
</ul>

---

## How will this work?

<ul>
  <li class="fragment">You will be assigned a (BIG) project</li>
    <ul>
    <li class="fragment">You will have to implement it</li>
    <li class="fragment">Work as a team</li>
    <li class="fragment">My role will be that of a mentor, more than a teacher</li>
    </ul>
</ul>

|||

## New skills?

<ul>
  <li class="fragment">[Git](https://git-scm.com/)</li>
  <li class="fragment">[PEP8](https://www.python.org/dev/peps/pep-0008/)</li>
  <li class="fragment">[Continuos integration](https://codeship.com/continuous-integration-essentials) and [Docker](https://docker.com)</li>
</ul>

---

## Picking a project

<ul>
  <li class="fragment">Remember all you learned up to now</li>
  <li class="fragment">Pick someting you enjoyed</li>
  <li class="fragment">Make it awesome</li>
</ul>

---

## References

 * [Git](https://git-scm.com/)
 * [PEP8](https://www.python.org/dev/peps/pep-0008/)
 * [Continuos integration](https://codeship.com/continuous-integration-essentials)
 * [Docker](https://docker.com)

